# Web Forms
-----------

![Web Forms](./logo.png)

## Documentation
-----------------
You can visit the docs of the sdk [here](http://webforms-javascript.bitballoon.com/).

Web actions in play.

## Action Props Types

|PropertyType | DataTypes   |
|-------------|-------------|
|   0         | STR         |
|   1         | ARRAY       |
|   2         | NUMBER      |
|   3         | BOOLEAN     |
|   3         | null        |(X)
|   4         | DATE        |
|   5         | IMAGE       |
|   6         | LINK        |
-----------------------------

## Features

* Supports all webforms methods.
* No need to write a backend server.
* Save data to webforms.
* Generate your forms instantly.
* Use custom API to write forms instantly.
