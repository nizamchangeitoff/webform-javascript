import config from '../config'

const name = config.set('name')
const client_id = config.set('client_id')
const redirect_uri = config.set('redirect_uri')

export const analyticSchema = {
  unixTimeStamp: String,
  redirect_uri: String,
  client_id: String,
  name: String
}

export const analyticsData = {
  unixTimeStamp: parseInt(new Date().getTime() / 1000),
  redirect_uri,
  client_id,
  name
}
