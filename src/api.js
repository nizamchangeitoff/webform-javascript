'use strict'
import config from './config'
import axios from 'axios'
import Reject from './reject'

/**
 * Lower Level API for web forms
 */
class API {
  constructor () {
    this.state = {
    }
    this.get = this.get.bind(this)
    this.post = this.post.bind(this)
    this.put = this.put.bind(this)
  }

  /**
     * Custom request Plugin
     * @param {Object} restSchema options for the middleware
     * @returns {Promise}
     */
  request (options = {}) {
    if (!options.url) return new Reject('Url required.')
    if (!options.method) return new Reject('method not provided')
    return axios(options)
  }

  /**
     * Get request for the api
     * @param {string} path
     * @param {Object} params
     * @param {function} progress
     */
  get (path = '', params = {}, progress = () => {}) {
    if (!path) {
      return new Reject('No path given. Please provide a webform path.')
    }
    const VERSION = config.get('api_version')
    const baseURL = config.get('baseURL')

    const auth = config.get('header_token') || '5812233c9ec6682dbce36860'
    const url = `${baseURL}/WebAction/${VERSION}${path}`
    const options = {
      // withCredentials: true,
      async: true,
      crossDomain: true,
      headers : {
        'Authorization': auth,
      },
      params,
      onDownloadProgress: progress
    }
    return axios.get(url, options)
  }

  /**
     * Post data to web forms. Save any details in a particular schema.
     * @param {string} path
     * @param {Object} params
     * @return {Promise|AxiosPromise|*}
     * @param progress
     * @param data
     */
  post (path, params = {}, data = {}, progress = () => {}) {
    if (!path) {
      return new Reject('No path given. Please provide a webform path.')
    }
    const VERSION = config.get('api_version')
    const baseURL = config.get('baseURL')
    const auth = config.get('header_token') || '5812233c9ec6682dbce36860'
    const url = `${baseURL}/WebAction/${VERSION}${path}`
    const options = {
      async: true,
      crossDomain: true,
      method: 'POST',
      headers: {
        authorization: auth,
        'content-type': 'application/json',
      },
      url,
      data
    }
    console.log(options)

    return axios(options)
  }

  /**
     * #### TODO | Not complete yet
     * Put request for the web form and the _id.
     * @param {string} path
     * @param {Object} body
     * @param {Object} query
     * @param {Object} params
     * @param {Object} progress
     * @return {null}
     */
  put (path, body = {}, query = {}, params = {}, progress = () => {}) {
    return null
  }
}

window.axios = axios
export default API
