// TODO: Deprecate this file.

import currentSchema from './currentSchema'

// 1) tranformSchemaToWebAction
// 2) tranformWebActionToSchema
// 1 -> 2
// 2 -> 1
// two-way function
export function tranformSchemaToWebAction (name, schema) {
  let props = []

  for (var key in schema) {
    if (schema.hasOwnProperty(key)) {
      const prop = {
        'DisplayName': key,
        'PropertyName': key,
        'PropertyType': parseType(schema[key]),
        'IsRequired': false,
        'ValidationRegex': null
      }
      props.push(prop)
    }
  }

  const webaction = {
    'Name' : name,
    'Properties' : []
  }
  webaction['Properties'] = props
  return webaction
}

export function parseType (schemaType) {
  switch (schemaType) {
    case Array:
      return 'STR'
    case String:
      return 'STR'
    case Number:
      return 'NUMBER'
    case Boolean:
      return 'BOOLEAN'
    default:
      return null
  }
}

export function tranformWebActionToSchema (WebAction) {
  let o = {}
  WebAction.Properties.forEach(function (item) {
    o[item.PropertyName] = parsePropType(item.PropertyType)
  })
  return o
}

export function parsePropType (propType) {
  switch (parseInt(propType)) {
    case 0:
      return 'String'
    case 1:
      return 'array'
    case 2:
      return 1
    case 3:
      return true
    case 4:
      return 'String'
    case 5:
      return { 'description': 'String', url: 'http://example.com/path/to/img.png' }
    case 6:
      return { 'description': 'String', url: 'http://example.com/path/to/img.png' }
    default:
      return 'String'
  }
}

/**
 * Saves WebformSchema to currentSchema
 * @param webFormSchema
 */
export function setSchema (webFormSchema) {
  // initial empty
  currentSchema.clear()
  for (var property in webFormSchema) {
    if (webFormSchema.hasOwnProperty(property)) {
      currentSchema.set(property, webFormSchema[property])
    }
  }
}

export function readSchema () {
  return currentSchema.read()
}
