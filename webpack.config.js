const webpack = require('webpack')
const path = require('path')
const OpenBrowserPlugin = require('open-browser-webpack-plugin')

const VERSION = require('./package.json').version
const IS_NPM = process.env.IS_NPM
const filePath = IS_NPM ? __dirname : path.join(__dirname, 'build', 'sdk')
const filename = IS_NPM ? 'sdk.js' : 'sdk-' + VERSION + '.js'
const htmlFilePath = path.join(__dirname, 'build', 'sdk', 'index.html')

module.exports = {
  devServer: {
    historyApiFallback: true,
    hot: true,
    inline: true,
    progress: true,
    contentBase: './build/sdk/',
    port: 8080
  },
  entry: [
    'webpack/hot/dev-server',
    'webpack-dev-server/client?http://localhost:8080',
    path.resolve(__dirname, 'src/main.js')
  ],
  output: {
    path: filePath,
    publicPath: '/',
    filename
  },
  module: {
    loaders: [
      { test: /\.js[x]?$/, include: path.resolve(__dirname, 'src'), exclude: /node_modules/, loader: 'babel-loader' }
    ]
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new OpenBrowserPlugin({ url: 'http://localhost:8080' })
  ]
}
